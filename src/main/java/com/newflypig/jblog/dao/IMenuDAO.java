package com.newflypig.jblog.dao;

import com.newflypig.jblog.model.Menu;

public interface IMenuDAO extends IBaseDAO<Menu>{
}
