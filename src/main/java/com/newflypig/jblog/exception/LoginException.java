package com.newflypig.jblog.exception;

import java.util.Calendar;

import javax.servlet.http.HttpSession;

import com.newflypig.jblog.model.BlogConfig;

public class LoginException extends JblogException {

	private static final long serialVersionUID = 3291706138122176605L;
	
	public LoginException(){
	}
	
	public LoginException(String message){
		super(message);
	}
	
	public LoginException(String message,HttpSession session){
		super(message);
		Integer post_num=(Integer)session.getAttribute(BlogConfig.POST_NUM);
		if(post_num==null)
			post_num=1;
		post_num++;
		session.setAttribute(BlogConfig.POST_NUM, post_num);
		//如果post_num>阀值，则向session中加入惩罚时间5分钟
		if(post_num>BlogConfig.PUNISH_NUM){
			Calendar c=Calendar.getInstance();
			c.set(Calendar.MINUTE, c.get(Calendar.MINUTE)+BlogConfig.PUNISH_MINUTE);
			session.setAttribute(BlogConfig.PUNISH_TIME, c.getTime());
		}
	}
}
