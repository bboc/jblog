package com.newflypig.jblog.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lianyihui", catalog = "jblog2")
public class Lianyihui implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	
	public static int TYPE_OTHER = 1;
	public static int TYPE_CODER = 2;
	public static int TYPE_DESIGNER = 3;
	public static int TYPE_SALER = 4;
	public static int TYPE_BOSS	 = 5;
	
	private Integer id;
	private String realname;
	private String phonenumber;
	private Integer type;
	private Date createDt;
	private String remark;
	private Integer showid;
	private String hash;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "realname")
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	
	@Column(name = "phonenumber")
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	@Column(name = "type")
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
	@Column(name = "createdt")
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	
	@Column(name = "remark")
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	@Column(name = "showid")
	public Integer getShowid() {
		return showid;
	}
	public void setShowid(Integer showid) {
		this.showid = showid;
	}
	
	@Column(name = "hash")
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
}
